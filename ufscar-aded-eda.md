---
title: 'Análise de Dados: Possíveis indicadores de doença no fígado'
author: "Beatriz Michellim 770918, Felipe Gusmao 632872, Giovani Verotti 770265, Paulo Anselmi 727956"
date: "12/16/2019"
output:
  html_document:
    keep_md: yes
  pdf_document: default
---





## Estrutura dos Dados

Este conjunto de dados contém  informações de pacientes com problemas no fígado e podem ser achados aqui, [Dados](https://gitlab.com/pingu1m/UFSCar-ADED-EDA/blob/master/Dados.xlsx)

Antes de fazer qualquer analise mais elaborada, e nescessario primeiro entender a *aparência dos dados* 

Primeiras observações com `head()`. 

### Variáveis
 1. Idade do paciente
 2. Sexo  do paciente
 3. Bilirrubina Total
 4. Bilirrubina direta
 5. Alcalina fosfatase
 6. Alamine Aminotransferase
 7. Aspartato aminotransferase
 8. Total de Proteinas
 9. Albumina
 10. Razão: Albumina/globulina
 11. Indicador da presença de doença no fígado


<table class="table table-condensed">
 <thead>
  <tr>
   <th style="text-align:right;"> ...1 </th>
   <th style="text-align:right;"> ...2 </th>
   <th style="text-align:right;"> ...3 </th>
   <th style="text-align:right;"> ...4 </th>
   <th style="text-align:right;"> ...5 </th>
   <th style="text-align:right;"> ...6 </th>
   <th style="text-align:right;"> ...7 </th>
   <th style="text-align:right;"> ...8 </th>
   <th style="text-align:right;"> ...9 </th>
   <th style="text-align:right;"> ...10 </th>
   <th style="text-align:right;"> ...11 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;"> 65 </td>
   <td style="text-align:right;"> Female </td>
   <td style="text-align:right;"> 0.7 </td>
   <td style="text-align:right;"> 0.1 </td>
   <td style="text-align:right;"> 187 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 6.8 </td>
   <td style="text-align:right;"> 3.3 </td>
   <td style="text-align:right;"> 0.90 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> Male </td>
   <td style="text-align:right;"> 10.9 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 699 </td>
   <td style="text-align:right;"> 64 </td>
   <td style="text-align:right;"> 100 </td>
   <td style="text-align:right;"> 7.5 </td>
   <td style="text-align:right;"> 3.2 </td>
   <td style="text-align:right;"> 0.74 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> Male </td>
   <td style="text-align:right;"> 7.3 </td>
   <td style="text-align:right;"> 4.1 </td>
   <td style="text-align:right;"> 490 </td>
   <td style="text-align:right;"> 60 </td>
   <td style="text-align:right;"> 68 </td>
   <td style="text-align:right;"> 7.0 </td>
   <td style="text-align:right;"> 3.3 </td>
   <td style="text-align:right;"> 0.89 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 58 </td>
   <td style="text-align:right;"> Male </td>
   <td style="text-align:right;"> 1.0 </td>
   <td style="text-align:right;"> 0.4 </td>
   <td style="text-align:right;"> 182 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 6.8 </td>
   <td style="text-align:right;"> 3.4 </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> Male </td>
   <td style="text-align:right;"> 3.9 </td>
   <td style="text-align:right;"> 2.0 </td>
   <td style="text-align:right;"> 195 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 7.3 </td>
   <td style="text-align:right;"> 2.4 </td>
   <td style="text-align:right;"> 0.40 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> Male </td>
   <td style="text-align:right;"> 1.8 </td>
   <td style="text-align:right;"> 0.7 </td>
   <td style="text-align:right;"> 208 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 7.6 </td>
   <td style="text-align:right;"> 4.4 </td>
   <td style="text-align:right;"> 1.30 </td>
   <td style="text-align:right;"> 1 </td>
  </tr>
</tbody>
</table>


```r
summary(df1) %>% kable() %>% kable_styling()
```

<table class="table" style="margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:left;">      ...1 </th>
   <th style="text-align:left;">     ...2 </th>
   <th style="text-align:left;">      ...3 </th>
   <th style="text-align:left;">      ...4 </th>
   <th style="text-align:left;">      ...5 </th>
   <th style="text-align:left;">      ...6 </th>
   <th style="text-align:left;">      ...7 </th>
   <th style="text-align:left;">      ...8 </th>
   <th style="text-align:left;">      ...9 </th>
   <th style="text-align:left;">     ...10 </th>
   <th style="text-align:left;">     ...11 </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Min.   : 4.00 </td>
   <td style="text-align:left;"> Length:583 </td>
   <td style="text-align:left;"> Min.   : 0.400 </td>
   <td style="text-align:left;"> Min.   : 0.100 </td>
   <td style="text-align:left;"> Min.   :  63.0 </td>
   <td style="text-align:left;"> Min.   :  10.00 </td>
   <td style="text-align:left;"> Min.   :  10.0 </td>
   <td style="text-align:left;"> Min.   :2.700 </td>
   <td style="text-align:left;"> Min.   :0.900 </td>
   <td style="text-align:left;"> Min.   :0.3000 </td>
   <td style="text-align:left;"> Min.   :1.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> 1st Qu.:33.00 </td>
   <td style="text-align:left;"> Class :character </td>
   <td style="text-align:left;"> 1st Qu.: 0.800 </td>
   <td style="text-align:left;"> 1st Qu.: 0.200 </td>
   <td style="text-align:left;"> 1st Qu.: 175.5 </td>
   <td style="text-align:left;"> 1st Qu.:  23.00 </td>
   <td style="text-align:left;"> 1st Qu.:  25.0 </td>
   <td style="text-align:left;"> 1st Qu.:5.800 </td>
   <td style="text-align:left;"> 1st Qu.:2.600 </td>
   <td style="text-align:left;"> 1st Qu.:0.7000 </td>
   <td style="text-align:left;"> 1st Qu.:1.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Median :45.00 </td>
   <td style="text-align:left;"> Mode  :character </td>
   <td style="text-align:left;"> Median : 1.000 </td>
   <td style="text-align:left;"> Median : 0.300 </td>
   <td style="text-align:left;"> Median : 208.0 </td>
   <td style="text-align:left;"> Median :  35.00 </td>
   <td style="text-align:left;"> Median :  42.0 </td>
   <td style="text-align:left;"> Median :6.600 </td>
   <td style="text-align:left;"> Median :3.100 </td>
   <td style="text-align:left;"> Median :0.9300 </td>
   <td style="text-align:left;"> Median :1.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Mean   :44.75 </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> Mean   : 3.299 </td>
   <td style="text-align:left;"> Mean   : 1.486 </td>
   <td style="text-align:left;"> Mean   : 290.6 </td>
   <td style="text-align:left;"> Mean   :  80.71 </td>
   <td style="text-align:left;"> Mean   : 109.9 </td>
   <td style="text-align:left;"> Mean   :6.483 </td>
   <td style="text-align:left;"> Mean   :3.142 </td>
   <td style="text-align:left;"> Mean   :0.9471 </td>
   <td style="text-align:left;"> Mean   :1.286 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> 3rd Qu.:58.00 </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> 3rd Qu.: 2.600 </td>
   <td style="text-align:left;"> 3rd Qu.: 1.300 </td>
   <td style="text-align:left;"> 3rd Qu.: 298.0 </td>
   <td style="text-align:left;"> 3rd Qu.:  60.50 </td>
   <td style="text-align:left;"> 3rd Qu.:  87.0 </td>
   <td style="text-align:left;"> 3rd Qu.:7.200 </td>
   <td style="text-align:left;"> 3rd Qu.:3.800 </td>
   <td style="text-align:left;"> 3rd Qu.:1.1000 </td>
   <td style="text-align:left;"> 3rd Qu.:2.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> Max.   :90.00 </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> Max.   :75.000 </td>
   <td style="text-align:left;"> Max.   :19.700 </td>
   <td style="text-align:left;"> Max.   :2110.0 </td>
   <td style="text-align:left;"> Max.   :2000.00 </td>
   <td style="text-align:left;"> Max.   :4929.0 </td>
   <td style="text-align:left;"> Max.   :9.600 </td>
   <td style="text-align:left;"> Max.   :5.500 </td>
   <td style="text-align:left;"> Max.   :2.8000 </td>
   <td style="text-align:left;"> Max.   :2.000 </td>
  </tr>
  <tr>
   <td style="text-align:left;">  </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> NA </td>
   <td style="text-align:left;"> NA's   :4 </td>
   <td style="text-align:left;"> NA </td>
  </tr>
</tbody>
</table>


```r
formattable(ExpData(data=df, type=2))
```


<table class="table table-condensed">
 <thead>
  <tr>
   <th style="text-align:right;"> S.no </th>
   <th style="text-align:right;"> Variable Name </th>
   <th style="text-align:right;"> Variable Type </th>
   <th style="text-align:right;"> % of Missing </th>
   <th style="text-align:right;"> No. of Unique values </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> Idade </td>
   <td style="text-align:right;"> numeric </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 72 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> Sexo* </td>
   <td style="text-align:right;"> factor </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> BilirrubinaTotal </td>
   <td style="text-align:right;"> numeric </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 113 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> BillirrubinaDireta </td>
   <td style="text-align:right;"> numeric </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 80 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> AlcalinaFosfatase </td>
   <td style="text-align:right;"> numeric </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 263 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> AlamineAminotransferase </td>
   <td style="text-align:right;"> numeric </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 152 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> AspartatoAminotransferase </td>
   <td style="text-align:right;"> numeric </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 177 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> TotalProteinas </td>
   <td style="text-align:right;"> numeric </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 58 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> Albumina </td>
   <td style="text-align:right;"> numeric </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 40 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> AlbuminaPorGlobulina </td>
   <td style="text-align:right;"> numeric </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 70 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> DoencaFigado* </td>
   <td style="text-align:right;"> factor </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 2 </td>
  </tr>
</tbody>
</table>


<table class="table table-condensed">
 <thead>
  <tr>
   <th style="text-align:right;"> Variable </th>
   <th style="text-align:right;"> Valid </th>
   <th style="text-align:right;"> Frequency </th>
   <th style="text-align:right;"> Percent </th>
   <th style="text-align:right;"> CumPercent </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:right;"> Sexo </td>
   <td style="text-align:right;"> Female </td>
   <td style="text-align:right;"> 142 </td>
   <td style="text-align:right;"> 24.36 </td>
   <td style="text-align:right;"> 24.36 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> Sexo </td>
   <td style="text-align:right;"> Male </td>
   <td style="text-align:right;"> 441 </td>
   <td style="text-align:right;"> 75.64 </td>
   <td style="text-align:right;"> 100.00 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> Sexo </td>
   <td style="text-align:right;"> TOTAL </td>
   <td style="text-align:right;"> 583 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:right;"> DoencaFigado </td>
   <td style="text-align:right;"> Doente </td>
   <td style="text-align:right;"> 416 </td>
   <td style="text-align:right;"> 71.36 </td>
   <td style="text-align:right;"> 71.36 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> DoencaFigado </td>
   <td style="text-align:right;"> Não doente </td>
   <td style="text-align:right;"> 167 </td>
   <td style="text-align:right;"> 28.64 </td>
   <td style="text-align:right;"> 100.00 </td>
  </tr>
  <tr>
   <td style="text-align:right;"> DoencaFigado </td>
   <td style="text-align:right;"> TOTAL </td>
   <td style="text-align:right;"> 583 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

# Introdução

Os sintomas da maioria das doenças que atacam o fígado são semelhantes, variando apenas na intensidade e velocidade em que aparecem.

Entre os principais sintomas de doenças do fígado estão: icterícia, urina escura, fezes claras, cansaço e, em quadros mais graves, distensão do abdome por acúmulo de líquido (barriga d’água), febre e sangramentos. Essas 
Doenças no figado muitas vezes são causadas por alterações em determinadas enzimas ou nutrientes do corpo, afetando em seu funcionando e alguns exames específicos auxiliam em um diagnóstico mais preciso e favorável. 

Algumas das enzimas e nutrientes analisados são:

## Bilirrubina
 É um produto da destruição das hemácias e para ser eliminada pelo organismo necessita ser conjugada a um açúcar no fígado e sofrer a ação da bile. Bilirrubina direta (ou conjugada) corresponde à conjugação entre a bilirrubina e o ácido glicurônico. Assim, a concentração de bilirrubina direta está alterada quando há alguma lesão hepática ou obstrução biliar.

| Tipo de Bilirrubina | Valor normal  |
|---------------------|---------------|
| Bilirrubina direta  | até 0,3 mg/dL |
| Bilirrubina total   | até 1,2 mg/dL |

## Alcalina
O exame da fosfatase alcalina serve para investigar distúrbios hepáticos ou ósseos e o seu resultado pode identificar Fosfatase alcalina alta
A fosfatase alcalina pode estar elevada quando existem problemas no fígado como: Obstrução do fluxo biliar, provocados por cálculos biliares ou câncer, que bloqueiam os canais que conduzem a bile para o intestino; Hepatite, que é uma inflamação no fígado que pode ser provocada por bactérias, vírus ou produtos tóxicos; Cirrose, que é uma doença que conduz à destruição do fígado; Consumo de alimentos gordurosos e Insuficiência renal.


## Alamine Aminotransferase
O exame da alanina aminotransferase, também conhecido como ALT ou TGP, é um exame de sangue que ajuda a identificar lesões e doenças do fígado devido à presença elevada da enzima alanina aminotransferase, também chamada de transaminase glutâmico pirúvica, no sangue, que normalmente se encontra entre as 7 e 56 U/L de sangue. Os resultados são considerados alterados quando:

| Resultado Alamine Aminotransferase | Indica                                                                                            |
|------------------------------------|---------------------------------------------------------------------------------------------------|
| 4 vezes superior ao normal         | Doença hepática como cirrose ou câncer                                                            |
| 10 vezes superior ao normal        | Hepatite aguda causada por vírus ou uso de alguns medicamentos                                    |
| 100 vezes superior ao normal       | Comum em utilizadores de drogas, álcool ou outras substâncias que causam danos graves no fígado\. |

## Aspartato Aminotransferase

O exame do aspartato aminotransferase ou transaminase oxalacética (AST ou TGO),é um exame de sangue solicitado para investigar lesões que comprometem o funcionamento normal do fígado, como hepatite ou cirrose, por exemplo. O valor de referência da enzima é entre 5 e 40 U/L de sangue, podendo variar de acordo com o laboratório.

## Total de Proteínas

A medida das proteínas totais no sangue reflete o estado nutricional da pessoa, e pode ser usada no diagnóstico de doenças renais, hepáticas e de outros distúrbios. Se os níveis de proteínas totais estiverem alterados, devem-se fazer outros testes para identificar qual a proteína específica que está alterada, para que possa ser feito o diagnóstico correto. Os valores de referência para pessoas com idade igual ou acima de 3 anos são: Proteínas totais: 6 a 8 g/dL, Albumina: 3 a 5 g/dL, Globulina: entre 2 e 4 g/dL.

| Proteína         | Valor normal |
|------------------|--------------|
| Proteínas Totais | 6 a 8 g/dL   |
| Albumina         | 3 a 5 g/dL   |
| Globulina        | 2 e 4 g/dL\. |


# Objetivo

A partir de determinadas análises o trabalho em questão tem o objetivo de estudar a relação entre a bilirrubina total, bilirrubina direta, alcalina fosfatase, alamine aminotransferase, aspartato aminotransferase, total de proteínas, albumina, a razão entre albumina e globulina e o acarretamento em uma possível doença no fígado.
 

# Métodos

Para auxiliar na leitura desses dados e em sua analise, foram gerados, gráficos e tabelas a fim de tornar mais fácil a interpretação das variáveis citadas anteriormente, analisadas de formas univariada, bivariada e multivariada, com a doença no fígado. Os métodos utilizados para analise foram: gráfico de barras; gráfico boxplot; regressões lineares; matriz scatter plot.

# Plots análise univariada 

## Variáveis Contínuas

### Histogramas

![](ufscar-aded-eda_files/figure-html/univariate_Plots-1.png)<!-- -->

### Boxplots com jitter


![](ufscar-aded-eda_files/figure-html/univariate_Plots3-1.png)<!-- -->

## Variáveis Categóricas
![](ufscar-aded-eda_files/figure-html/univariate_Plots2-1.png)<!-- -->

## Estrutura dos dados

 - Albumina: É um dado simétrico, onde não possui nenhum dado outlier
 - Alcalina Fosfatase: Possui uma assimetria em seus dados, tendo alguns dados maiores  que seu limitante        superior. sendo assimétrica à direita indicando que tem valores superiores a tendência.
 - Bilirrubina Total é semelhante a Bilirrubina Direta onde a maior porcentagem dos valores se concentram        próximos ao primeiro quartil, tendo uma assimetria para a direita. Entretanto sua média acaba sendo puxada    para valores maiores devido ao número de outlier’s 
   Em relação as aminotransferases tanto a Alamine quanto o Aspartato elas também possuem uma assimetria para    a direita, entretanto a alamine possui uma quantidade de outilier mais densa perante o Aspartato
 - Proteínas: Possuem uma assimetria para a esquerda, tendo a maior concentração de outlier antes do limitante    inferior, é notório também que a maior densidade de valores se concentram em medidas centrais.
 - Idade: todas possuem medidas onde não possuem assimetria se concentram em medidas centrais
 - Albumina por Globumina: possui uma assimetria para a direita sua densidade se apresenta entre valores de q1    e MD. os outliner aparecem superior à cima ao limitante superior.

## Características de interese na base de dados

As características de interesse nessa base de dados são os resultados obtidos em cada exame e coleta de informações sobre a quantidade de enzimas e nutrientes presentes no corpo dos pacientes com possível doença no fígado. 

# Plots análise bivariada 
![](ufscar-aded-eda_files/figure-html/Bivariate_Plots-1.png)<!-- -->![](ufscar-aded-eda_files/figure-html/Bivariate_Plots-2.png)<!-- -->![](ufscar-aded-eda_files/figure-html/Bivariate_Plots-3.png)<!-- -->![](ufscar-aded-eda_files/figure-html/Bivariate_Plots-4.png)<!-- -->

# Análise Bivariada

## Características observadas nos gráficos.

Com os boxplots é possível observar que os pacientes doentes apresentam uma elevada concentração de Bilirrubina Total e Bilirrubina direta de acordo com os resultados dos exames obtidos se comparado com os pacientes não doentes e em ambos os resultados há muitos outliers. Já ao analisar a razão de albumina/globulina, nota-se que a diferença não é tão discrepante ao comparar os resultados dos pacientes doentes e dos não doentes.

## Outra observação

Ao correlacionar a variável Sexo com o resultado de se ter doença no fígado, foi observado que não há uma relação nítida quanto a isso. No atual banco de dados, ao analisar os pacientes doentes, percebe-se que a maior parte são do sexo masculino. Porém, ao analisar de forma individual cada sexo, nota-se que a proporção de mulheres doentes é bem próximo à proporção de homens doentes. Com isso, não há relação direta entre o sexo e estar doente. 

## Observações significativas

Analisando os 3 boxplot, vemos uma grande discrepância no gráfico de bilirrubina direta,onde temos uma grande densidade de pessoas que possuem doença localizadas perto do primeiro quartil e da mediana. e uma grande quantidade de pessoas possuem uma discrepância com taxa extremamente elevadas, sendo em alguns casos muito maior que o limitante superior. Já nos que não possuem doença é uma relação relativamente simétrica porém mesmo tendo valores discrepantes são relativamente pequenos . Comparando as duas vemos que os valores presentes nos não doentes não são de suma importância para interferir nos doentes, tanto que o maior valor do outlier é menor que o limitante superior  dos doentes

# Plots análise multivariada 

## Matriz de Scatterplots e correlação

![](ufscar-aded-eda_files/figure-html/Multivariate_Plots-1.png)<!-- -->

## Scatterplots e regressões

![](ufscar-aded-eda_files/figure-html/unnamed-chunk-5-1.png)<!-- -->![](ufscar-aded-eda_files/figure-html/unnamed-chunk-5-2.png)<!-- -->

# Análise Multivariada

## Características observadas

O Correlograma indica uma correlação forte e positiva somente entre as variáveis Bilirrubina Direta com Bilirrubina total e Aspartato Aminotransferase com Alamine Aminotransferase. 

## Relações interesante entre características

Analisando com mais detalhes estas relações, pode-se notar um comportamento linear, ou seja, um alto nível de Bilirrubina Total implica em um alto nível de Bilirrubina Direta. Da mesma forma, há uma associação linear entre Alamine Aminotransferase e Aspartato Aminotransferase.
portadores da doença.

## Relação mais forte

Em ambos acima citados os portadores de doença possuem fortes relações lineares. Póis pacientes com Alamine Aminotransferase alto, possuem também Aspartato Aminotransferase alto.


# Referências

- NASCIMENTO, L. F.  A Sociologia Digital: um desafio para o século XXI. Sociologias, Porto Alegre, ano 18, n 41, jan/abr 2016. Disponível em: <http://www.scielo.br/pdf/soc/v18n41/1517-4522-soc-18-41-00216.pdf> Acesso em: 10 jun. 2018.
- Hands-On Exploratory Data Analysis with R 0 REVIEWS, by Harish Garg, Radhika Datar Publisher: Packt           Publishing Release Date: May 2019 ISBN: 9781789804379 Topic: Exploratory data analysis
  Disponível em: https://datascienceguide.github.io/exploratory-data-analysis
- Graphical Data Analysis with R 1 REVIEW, by Antony Unwin Publisher: Chapman and Hall/CRC Release Date:        September 2018ISBN: 9781315360041
  Disponível em: https://datascienceguide.github.io/exploratory-data-analysis
- DAROCZING. Export a matrix to Excel , Feb 2015
  Disponível em: https://stackoverflow.com/questions/5003371/export-a-matrix-to-excel
- SERPEJANTE, C. Fosfatase alcalina: exame investiga exames no fígado e nos ossos.
  Disponível em: https://www.minhavida.com.br/saude/tudo-sobre/18546-fosfatase-alcalina-sangue
- Brown J. Qual é de fato a quantidade de proteínas que nosso corpo precisa. BBC Brasil, 17 junho 2018.
  Disponível em: https://www.bbc.com/portuguese/vert-fut-44378545
- DRA. GEOVANA, N. Para que serve os exames de bilirrubina do sangue?, 1 novembro 2018.
  Disponível em: https://medicoresponde.com.br/para-que-serve-o-exame-de-bilirrubina-no-sangue/
- LEMOS, M. Como entender o exame de TGO-AST: Aspartato Aminotransfarase. Julho 2019.
  Disponível em: https://www.tuasaude.com/exame-aspartato-aminotransferase/
- LEMOS, M.Para que serve o exame de albumina. Novembro, 2018.
  Disponível em :https://www.tuasaude.com/exame-de-albumina/
- SOUSA, G. A albumina e a globulina na detecção de doenças no fígado. Médico especializado em Patologia        Clínica. Disponível em: https://lifestyle.sapo.pt/saude/saude-e-medicina/artigos/a-albuminaglobulina-na-detecao-de-doencas-no-figado
